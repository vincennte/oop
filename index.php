<?php 
require('ape.php');
require('frog.php');

$sheep = new Animal("shaun");

echo "Name : ".$sheep->name."<br>"; // "shaun"
echo "Legs: ".$sheep->legs."<br>"; // 4
echo "Cold blooded: ".$sheep->cold_blooded."<br>"; // "no"
echo "<br>";
// NB: Boleh juga menggunakan method get (get_name(), get_legs(), get_cold_blooded())

$kodok = new Frog("buduk");

echo "Name : ".$kodok->name."<br>"; // "shaun"
echo "Legs: ".$kodok->legs."<br>"; // 4
echo "Cold blooded: ".$kodok->cold_blooded."<br>"; // "no"
echo "Jump: ".$kodok->jump."<br>"; // "no"
echo "<br>";

$sungokong = new Ape("kera sakti");

echo "Name : ".$sungokong->name."<br>"; // "shaun"
echo "Legs: ".$sungokong->legs."<br>"; // 4
echo "Cold blooded: ".$sungokong->cold_blooded."<br>"; // "no"
echo "Yell: ".$sungokong->yell."<br>"; // "no"
echo "<br>";

?>